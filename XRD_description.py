import numpy as np 
import matplotlib.pyplot as plt
import re
from bs4 import BeautifulSoup

class XRD_info:
    def __init__(self, file_name, compound_name, process_name):
        self.file_name =  file_name
        self.compound = compound_name
        self.process_name = process_name
        
        self.file = self.read_file()
        self.start_angle = self.get_angles()[0]
        self.end_angle = self.get_angles()[1]
        self.intensities = self.intensity_values()

    def read_file(self):
        with open(self.file_name, 'r') as file:
            document = file.read()
        
        soup = BeautifulSoup(document, 'html.parser')
        
        return soup
    
    def get_angles(self):
        angles = []

        file = self.file
        start_tag = file.positions.startposition
        end_tag = file.positions.endposition
        angles.append(float(start_tag.string))
        angles.append(float(end_tag.string))

        return angles
    

    def intensity_values(self):

        file = self.file
        tag_intensities = file.intensities
        intensities_str = tag_intensities.string
        intensities_str = re.split(" |\n", intensities_str)
        intensities = [int(value) for value in intensities_str]
        
        return intensities
        
    def plot(self):
        self.angles = np.linspace(self.start_angle, self.end_angle, 
                                len(self.intensities))

        fig, ax = plt.subplots(1, 1, figsize=(15, 4))

        plt.plot(self.angles, self.intensities,color='#244747')

        fig.text(0.4, 0.96, 'XRD Pattern of '+ self.compound, 
         fontsize=15, fontweight='bold', fontfamily='serif')
        fig.text(0.46, 0.9, self.process_name, 
         fontsize=12, fontweight='light', fontfamily='serif')
        plt.xlabel(r"2$\theta$ (deg.)")
        plt.ylabel("Intensity (a.u.)")

        plt.show()


if __name__ == "__main__":
    xrd = XRD_info('2. CaLaTiFeO6-.XRDML', 'CaLaTiFeO6', 'Burn')
    
    print(xrd.get_angles())
    xrd.plot()

